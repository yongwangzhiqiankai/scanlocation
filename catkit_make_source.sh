#!/bin/bash
#sukai 2022-04-21
# source catkit_make_source.sh
catkin_make --pkg contnav_msgs  
catkin_make --pkg contnav_srvs
catkin_make
if [[ $? != 0 ]]; then
echo 'catkin_make :error'
else
echo 'catkin_make :ok'
source devel/setup.bash
echo 'source devel/setup.bash :ok'
fi
