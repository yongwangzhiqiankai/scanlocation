
基于图片匹配算法做的重定位
作者：再遇当年
日期：2022-12-17
系统环境：
    ubuntu: 20.04
    ros: noetic
    opencv4.5.0
地图路径：
   /home/sukai/workspace/scanlocation/src/scanlocation/output/image
   使用我给的地图跑仿真
需要代码走读请联系本人 
       422168787@qq.com
视频演示地址：
  https://b23.tv/yZkEoW2 
  
编译 :
   source catkit_make_source.sh 
 
启动：
   roslaunch scanlocation   scan_to_scan_pose.launch

         接受用户指令
         ros::ServiceClient    ScanToPointToPngClient = node_handle_.serviceClient<contnav_srvs::ScanToPointToPng::Request, contnav_srvs::ScanToPointToPng::Response>("/ScanToPointToPng");
         用户：
                request_type :

                             initialposeStatic
                             initialposeStaticthread
                             initialposeDynamic
                             initiallocation
                             initialposeStop

             返回：
                   string result：ok 成功  error 失败
                   string message


          发布融合定位的topic：    /scanTpngPose
          geometry_msgs::PoseStamped








